export interface IOffer {
    id?: number;
    title: string;
    providerId: string;
    receiverId?: string;
    description: string;    
    date: string;
    likes: number;
    images: string;
    category:string;
}



export class Offer implements IOffer {
    constructor(     
        public providerId: string,
        public title: string,
        public description: string,
        public date: string,
        public likes: number,
        public images: string, 
        public category: string,
        public receiverId?: string,
        public id?: number
        ) {}
  
}
