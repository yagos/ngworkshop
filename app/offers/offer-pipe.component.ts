import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
    name: 'offerFilter'
})

export class OfferFilterPipe implements PipeTransform{

    transform(value: any[], filterBy: string): any[]{
        let filter: string = filterBy ? filterBy.toLowerCase() : null;
        return filter ? value.filter((offer: any) =>
            offer.title.toLocaleLowerCase().indexOf(filter) !== -1) : value;   
    }
}