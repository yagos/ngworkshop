import { Component } from '@angular/core';
import { IOffer } from './offer';
import { OfferService} from './offer-service.component';
import {OfferFilterPipe} from './offer-pipe.component'

@Component({
    selector: 'offer-list',
    templateUrl: 'app/offers/offers-list.component.html',
    styleUrls: ['app/offers/offer-list.component.css']
})

export class OfferList{
    pageTitle: string = "Hello hello";
    offers: IOffer[];
    offerParam: string;
    errorMessage: string;


    constructor(private _offerService: OfferService){
         this._offerService.getOffers().
        subscribe(offers => this.offers = offers,
            error => this.errorMessage = <any>error);
        console.log("getting offers is ready!");
    }

}