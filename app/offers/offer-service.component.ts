import {Injectable} from '@angular/core';
import {IOffer} from './offer';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Http, Response, Headers} from '@angular/http';


@Injectable() 
export class OfferService{
    offers: IOffer[];
    private _offersUrl = 'http://localhost:8080/rest/api/v1/offer/list';
    private _offerUrl ='http://localhost:8080/rest/api/v1/offer/';
    private _offerNewUrl ='http://localhost:8080/rest/api/v1/offer';
    private _offerUpdate = 'http://localhost:8080/rest/api/v1/offer/';
    private header:Headers;
    private offer:IOffer;
    

    constructor(private _http: Http) {
        this.header = new Headers(); 
        this.header.append('Content-Type', 'application/json');
    }

    
        getOffers(): Observable<IOffer[]> {
        //The map function processes a result from the observable
        //(in our case, the fetched payload of an http.get call), using an ES6 arrow function.
        //The arrow function's parameter res is actually a ResponseData object, 
        //and can be parsed as binary (blob()), string (text()) or, in our case, 
        //JavaScript via JSON (json()) content via its helper methods.
        //So in this way, we're already transforming the output from a raw HTTP 
        //Response to a JavaScript object.
        return this._http.get(this._offersUrl,{headers:this.header}).
            map((response: Response) => <IOffer[]>response.json()).
            do(data => console.log('All: ' + JSON.stringify(data))).
            catch(this.handleError); 
    }

     private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || "Something went wrong!");
    }
}