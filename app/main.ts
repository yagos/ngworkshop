import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';  // how should angular handle our application.
import { AppModule } from './app.module';
// functions that bootstrap our module
const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);
