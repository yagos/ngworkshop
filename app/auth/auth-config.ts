interface AuthConfiguration {
    clientID: string,
    domain: string
}

export const myConfig: AuthConfiguration = {
       clientID: 'hEcjw5YhgF6Bvm354nkR28hjiu0AbRdC',
    domain: 'scandio.auth0.com'
};
