// Our main component
import { AppComponent } from './app.component';

// Own components
import { ProfileEdit } from './profile/profile_edit.component';
import {OfferList} from './offers/offer-list.component';
import {OfferService} from './offers/offer-service.component';
import {OfferFilterPipe} from './offers/offer-pipe.component'

// Ng-Components
import { NgModule }      from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';


// Third-parties libraries/components
import { Auth } from './auth/auth.service';
import { AUTH_PROVIDERS }      from 'angular2-jwt';

// declarations refer to the components that belong to this module.
// imports define external modules that we want to have available on this app. Modules provided by Angular, thirdparties, or other modules components.
// BrowserModule must be imported by all browser applications. Register important service providers such as error handling. 
// BrowserModule also is used to expose Built-in directives.
// bootstrap defines the root component of the application, the root component should contains the directive defined in the index.html file.   
// FormsModule is required to use the ngModel within templates. 
@NgModule({
  imports: [ BrowserModule, 
  RouterModule.forRoot( [{path: 'profile', component: ProfileEdit}, {path: '', component: OfferList}] ), FormsModule, HttpModule 
    ],
  declarations: [AppComponent, ProfileEdit, OfferList, OfferFilterPipe],
  bootstrap:    [ AppComponent ],
  providers: [Auth, AUTH_PROVIDERS, OfferService]
})
export class AppModule { 
  
}

