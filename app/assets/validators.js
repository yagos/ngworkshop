$('#offerFormular')
  .form({
    on: 'blur',
    fields: {
      title: {
        identifier  : 'title',
        rules: [
          {
            type   : 'text',
            prompt : 'Please enter a title'
          }
        ]
      }
    }
  })
;