$('#gift-items .image')
  .dimmer({
    on: 'hover'
  })
;

$('#country-dd')
  .dropdown()
;
$('#avatar-dd')
  .dropdown()
;
$('#genre-dd')
  .dropdown()
;

// Shape
$('.shape').shape();

$(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
      ;

    })
  ;