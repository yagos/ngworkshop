import { Component }            from '@angular/core';
import { Http } from '@angular/http';
import { Auth }                 from '../auth/auth.service';
import { AuthHttp }             from 'angular2-jwt';
import { Router }               from '@angular/router';
import 'rxjs/add/operator/map';
import { myConfig }             from '../auth/auth-config';

@Component({
  //selector: 'profile',
  templateUrl: 'app/profile/profile_edit.template.html'
})

export class ProfileEdit {
  address: string;
  firstName: string;
  lastName: string;
  genre: string;
  nationality: string;
  username: string;
  success: boolean;
  failure: boolean;

  constructor(private auth: Auth, private authHttp: AuthHttp, private _router: Router) {
    if(auth.userProfile && auth.userProfile.user_metadata){
      this.address = auth.userProfile.user_metadata.address;
      this.firstName = auth.userProfile.user_metadata.firstName;
      this.lastName = auth.userProfile.user_metadata.lastName;
      this.genre = auth.userProfile.user_metadata.genre;
      this.nationality = auth.userProfile.user_metadata.nationality;
      this.username = auth.userProfile.user_metadata.username;

    }
  }

  onSubmit() {
    var headers: any = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };

    var data: any = JSON.stringify({
      user_metadata: {
        address: this.address,
        firstName: this.firstName,
        lastName: this.lastName,
        genre: this.genre,
        nationality: this.nationality,
        username: this.username
      }
    });

    this.authHttp
      .patch('https://' + myConfig.domain + '/api/v2/users/' + this.auth.userProfile.user_id, data, {headers: headers})
      .map(response => response.json())
      .subscribe(
        response => {
          this.auth.userProfile = response;
          localStorage.setItem('profile', JSON.stringify(response));
          this._router.navigate(['/profile']);
          this.success = true;
        },
        error =>{ alert(error.json().message); this.failure = true}
      );
  }

   onBack(): void{
        this._router.navigate(['']);    
    }

  isOkay(){
    return this.success;
  }

  isBad(){
    return this.failure;
  }
}
