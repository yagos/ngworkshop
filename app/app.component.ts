import {Component, OnInit} from '@angular/core';
import { Auth } from './auth/auth.service';

// Decorator
@Component({
    selector:'ms-app', 
    templateUrl:'app/app.component.html',
    styleUrls: ['./app/assets/app-component.css']
})

// Class
export class AppComponent{
    // Properties
    pageTitle: string = 'ng-Workshop';
    constructor(private auth: Auth){
    }
}

